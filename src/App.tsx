import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import CodeEditor from './CodeEditor';

function App() {
  const [code, setCode] = useState("# python code goes here")
  
  const changeCode = () => {
    console.log("code changed.")
  }

  return (
    <div className="App">
      <header className="App-header">
      <CodeEditor
            height="50vh"
            initValue={code}
            language={"python"}
            onChange={changeCode}
          />
      </header>
    </div>
  );
}

export default App;
